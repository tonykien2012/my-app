FROM wyveo/nginx-php-fpm:php81
COPY index.php /usr/share/nginx/html
ADD default.conf /etc/nginx/conf.d/default.conf
